/*
    SimpleSerial index.js
    Created 7 May 2013
    Modified 9 May 2013
    by Tom Igoe
    */
var actionsList = []; //array qye recebe os comandos
var mySound = null; 
var mySound2 = null;

var app = {
    macAddress: "00:13:12:25:42:38",  // endereço do bluetoothSerial.list
    chars: "",

/*
    Application constructor
    */
    initialize: function() {
        this.bindEvents();
        console.log("Iniciando app");
    },
/*
    bind any events that are required on startup to listeners:
    */
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        connectButton.addEventListener('touchend', app.manageConnection, false);
        init.addEventListener('touchend', app.mover, false);
    },

/*
    this runs when the device is ready for user interaction:
    */
    onDeviceReady: function() {
	// variaveis de som
	mySound = new Media("/android_asset/www/som/pegar.mp3");
	mySound2 = new Media("/android_asset/www/som/soltar.mp3");

        // check to see if Bluetooth is turned on.
        // this function is called only
        //if isEnabled(), below, returns success:
        var listPorts = function() {
            // list the available BT ports:
            bluetoothSerial.list(
                function(results) {
                },
                function(error) {
                    app.display(JSON.stringify(error));
                }
                );
        }

        // if isEnabled returns failure, this function is called:
        var notEnabled = function() {
            app.display("Bluetooth nao esta habilitado.")
        }

         // check if Bluetooth is on:
         bluetoothSerial.isEnabled(
            listPorts,
            notEnabled
            );
     },
/*
    Connects if not connected, and disconnects if connected:
    */
    manageConnection: function() {

        // connect() will get called only if isConnected() (below)
        // returns failure. In other words, if not connected, then connect:
        var connect = function () {
            // if not connected, do this:
            // clear the screen and display an attempt to connect
            app.clear();
            // attempt to connect:
            bluetoothSerial.connect(
                app.macAddress,  // device to connect to
                app.openPort,    // start listening if you succeed
                app.showError    // show the error if you fail
                );
        };

        // disconnect() will get called only if isConnected() (below)
        // returns success  In other words, if  connected, then disconnect:
        var disconnect = function () {
            // if connected, do this:
            bluetoothSerial.disconnect(
                app.closePort,     // stop listening to the port
                app.showError      // show the error if you fail
                );
        };

        // here's the real action of the manageConnection function:
        bluetoothSerial.isConnected(disconnect, connect);
    },
/*
    subscribes to a Bluetooth serial listener for newline
    and changes the button:
    */
    openPort: function() {
        // if you get a good Bluetooth serial connection:
        // change the button's name:
        connectButton.innerHTML = "Desconectar";

        // set up a listener to listen for newlines
        // and display any new data that's come in since
        // the last newline:
        bluetoothSerial.subscribe('\n', function (data) {
            app.clear();
        });
        
    },

/*
    unsubscribes from any Bluetooth serial listener and changes the button:
    */
    closePort: function() {
        // if you get a good Bluetooth serial connection:
        // change the button's name:
        connectButton.innerHTML = "Conectar";
        // unsubscribe from listening:
        bluetoothSerial.unsubscribe(
            function (data) {
            },
            app.showError
            );
    },
/*
    appends @error to the message div:
    */
    // showError: function(error) {
    //     app.display(error);
    // },

/*
    appends @message to the message div:
    */
    display: function(message) {
        var display = document.getElementById("message"), // the message div
            lineBreak = document.createElement("br"),     // a line break
            label = document.createTextNode(message);     // create the label

        display.appendChild(lineBreak);          // add a line break
        display.appendChild(label);              // add the message node
    },
/*
    clears the message div:
    */
    clear: function() {
        var display = document.getElementById("message");
        display.innerHTML = "";
    },


    mover: function(){
      bluetoothSerial.write(actionsList);
  },

}; // end of app

